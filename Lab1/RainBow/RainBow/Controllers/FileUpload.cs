﻿using System.Security.AccessControl;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace RainBow.Controllers;

[ApiController]
public class FileUpload : Controller
{
    [HttpPost("save")]
    public async Task<IActionResult> SaveFile()
    {
        var fileName = Guid.NewGuid();
        
        do
        {
            fileName = Guid.NewGuid();
            
        } while (System.IO.File.Exists(Path.Combine("Files", $"{fileName.ToString()}.txt")));
        
        var filePath = Path.Combine("Files", $"{fileName.ToString()}.txt");
        
        using var sr = new StreamReader(Request.Body, Encoding.UTF8);

        var str = await sr.ReadToEndAsync();
        
        await System.IO.File.WriteAllTextAsync(filePath,str);
        
        return Ok(new Dictionary<string,string>()
        {
            {"fileName",fileName.ToString()}
        });
    }
    
    [HttpGet("upload")]
    public IActionResult GetFile(string fileCode)
    {
        var filePath = Path.Combine("Files", $"{fileCode}.txt");
        if (!System.IO.File.Exists(filePath))
        {
            return BadRequest("Fail not exits");
        }

        var fileContent = System.IO.File.ReadAllBytes(filePath);
        
        return File(fileContent, "plain/text", $"{fileCode}.txt");
    }
}