using Microsoft.Net.Http.Headers;
using Vite.AspNetCore;
using Vite.AspNetCore.Extensions;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddViteServices();

builder.Services.AddSpaStaticFiles(conf =>
{
    conf.RootPath = "clientApp/build";
});

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "MyAllowSpecificOrigins",
        policy =>
        {
            policy.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});


builder.Services.AddViteServices(options: new ViteOptions()
{
    Server = new ViteServerOptions()
    {
        AutoRun = true,
        Port = 5173,
        ScriptName = "dev",
        TimeOut = 5
    },
    PackageDirectory = "clientApp",
});

var app = builder.Build();


app.MapControllers();

app.UseSpaStaticFiles();


app.UseCors("MyAllowSpecificOrigins");


app.UseSpa(spa =>
{
    spa.Options.SourcePath = "clientApp";
    spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions()
    {
        OnPrepareResponse = ctx =>
        {
            var headers = ctx.Context.Response.GetTypedHeaders();
            headers.CacheControl = new CacheControlHeaderValue()
            {
                NoCache = true,
                NoStore = true,
                MustRevalidate = true
            };
        }
    };
    
    if (app.Environment.IsDevelopment())
    {
        app.UseViteDevMiddleware();
    }
});

app.Run();