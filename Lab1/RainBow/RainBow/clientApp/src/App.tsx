import MenuBar from "./components/ToolBars/MenuBar.tsx";
import ToolBar from "./components/ToolBars/ToolBar.tsx";
import TextArea from "./components/EditorBody/TextArea.tsx";
import {useAppDispatch, useTypedSelector} from "./store/hooks/hooks.ts";
import React from "react";
import {setMousePosition} from "./store/slices/textSlice.ts";
import rainBow from './assets/rainbow.png'
import smoke from './assets/smoke.png'

function App() {

    const {editorColor} = useTypedSelector(s => s.text);

    const dispatch = useAppDispatch();

    const {mousePosition} = useTypedSelector(s=>s.text);

    function handleMouseMove(e:React.MouseEvent<HTMLDivElement>){
        dispatch(setMousePosition({
            y:e.clientY,
            x:e.clientX
        }));
    }

    return (
        <>
            <div onMouseMove={(e)=>handleMouseMove(e)} className="editor" style={{backgroundColor: `${editorColor}`,position:"relative"}}>
                <div className="logo">
                    <span style={{fontFamily:"croisant",fontSize:"25px"}}>Twiligio editor</span>
                    <div style={{marginBottom:'8px'}}>
                        <img className="img-45" src={rainBow} alt=""/>
                        <img className="img-45" src={smoke} alt=""/>
                    </div>
                </div>
                <MenuBar/>
                <ToolBar/>
                <TextArea/>
                <div style={{display:"flex",padding:"10px",flexDirection:"column",gap:"5px",position:"absolute",bottom:"3px",right:"5px"}}>
                    <span style={{fontSize:"15px"}}>x:{mousePosition?.x}</span>
                    <span style={{fontSize:"15px"}}>y:{mousePosition?.y}</span>
                </div>
            </div>
        </>
    )
}

export default App
