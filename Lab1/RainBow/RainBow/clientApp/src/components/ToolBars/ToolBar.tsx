import './ToolBar.css'
import ClipBoard from "../ClipBoard/ClipBoard.tsx";
import Fonts from "../FontsSection/Fonts.tsx";
import ParagraphSection from "../ParagraphSection/ParagraphSection.tsx";


const ToolBar = () => {



    return (
        <div className="tool-bar">
            <ClipBoard/>
            <Fonts/>
            <ParagraphSection />
        </div>
    );
};

export default ToolBar;
