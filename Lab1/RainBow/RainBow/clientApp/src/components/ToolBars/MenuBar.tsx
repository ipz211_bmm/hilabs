import './MenuBar.css'
import {ChangeEvent, useEffect, useState} from 'react';
import {useAppDispatch, useTypedSelector} from "../../store/hooks/hooks.ts";
import {getText, replaceTextAct, setEditorColor, setText, textSearch} from "../../store/slices/textSlice.ts";
import settingsFileImg from '../../assets/docset.png'
import openFile from '../../assets/openFile.png'
import saveFile from '../../assets/download.png'
import arrow from '../../assets/undo.png'
import printImg from '../../assets/printer.png'
import settingImg from '../../assets/settings.png'
import closImg from '../../assets/close.png'
import axios from "axios";
import {readFileContent} from "../../utils/files.ts";

interface SaveFileResponse{
    fileName:string;
}
const MenuBar = () => {

    const [visible,setVisible] = useState<boolean>(false);

    const {text,getText:getter,editorColor} = useTypedSelector(t=>t.text);

    const [guid,setGuid] = useState<string>("");

    const [replaceText,setReplaceText] = useState<string>("");
    const [searchValue,setSearchText] = useState<string>("");

    const dispatch = useAppDispatch();
    function handleSettingsClick(){
        setVisible(!visible);
    }

    async function handleFileChange(e:ChangeEvent<HTMLInputElement>){

        if(e.target.files!==null){
            const file = e.target.files[0];
            if(file){
                const textContent:string = await readFileContent(file) as string;
                dispatch(setText(textContent));
                e.target.value='';
            }
        }
    }

    useEffect(() => {
        if(getter&&getter.isFileUploading&&text!==""){

            axios.post<SaveFileResponse>("http://localhost:5210/save",text, {
                   headers:{
                       "Content-type":"text/plain",
                   },
                }).then(res=>{
                if(res){
                    dispatch(getText({
                        isFileUploading:false
                    }));
                    setGuid(res.data.fileName);
                }
            }).catch(err=>{
                console.log(err);
                setGuid("");
                dispatch(getText({
                    isFileUploading:false
                }));
            });
        }

    }, [getter,text]);

    useEffect(() => {
        if(guid!==""){
            window.location.href=`/upload?fileCode=${guid}`;
        }
    }, [guid]);


    function handleSaveFile(){
        dispatch(getText({
            isFileUploading:true
        }));
    }

    function handleUndoClick(){
        dispatch(getText({
            isFileUploading:false,
            isUndoAction:true
        }));
    }

    function handleColorChange(e:ChangeEvent<HTMLInputElement>){
        dispatch(setEditorColor(e.target.value));
    }

    return (
        <div className="menu-bar">
            <div className="options-wrapper">
                <img onClick={handleSettingsClick} className="file-settings" src={settingsFileImg} alt="seeting"/>
                <div className="options" style={{display:`${visible?"flex":'none'}`}}>
                    <div onClick={handleSaveFile} className="option-item">
                        <img className="img-20" src={saveFile} alt="open file"/>
                        <span>Save file</span>
                    </div>
                    <label htmlFor="file" className="option-item">
                        <img className="img-20" src={openFile} alt="opend file"/>
                        <span>Open file</span>
                        <input onChange={(e)=>handleFileChange(e)} id='file' accept=".txt" name='file' style={{display:'none'}} type="file"/>
                    </label>
                    <div className="option-item">
                        <img className="img-20" src={printImg} alt="open file"/>
                        <span>Print</span>
                    </div>
                    <div className="option-item">
                        <img className="img-20" src={settingImg} alt="open file"/>
                        <span>Properties</span>
                    </div>
                    <div className="option-item">
                        <img className="img-20" src={closImg} alt="open file"/>
                        <span>Close</span>
                    </div>
                </div>
                <div className="items-group">
                    <img onClick={handleUndoClick} className="file-settings" src={arrow} alt=""/>
                    <img className="file-settings" style={{transform:"scaleX(-1)"}} src={arrow} alt=""/>
                </div>
                <div className="colors">
                    <input onChange={(e)=>{
                        setSearchText(e.target.value);
                    }} type="text" placeholder="Search value" value={searchValue} className="input-search"/>
                    <input onChange={(e)=>{
                       setReplaceText(e.target.value);
                    }} type="text" placeholder="Enter change value" value={replaceText} className="input-search"/>
                    <button onClick={()=>{
                        dispatch(textSearch(searchValue));
                        dispatch(replaceTextAct(replaceText));
                    }} className="input-search">Replace</button>
                    <input onChange={(e)=>handleColorChange(e)}  value={editorColor} type="color"/>
                </div>
            </div>
        </div>
    );
};

export default MenuBar;
