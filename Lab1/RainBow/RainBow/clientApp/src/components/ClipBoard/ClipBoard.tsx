import "./ClipBoard.css"
import pasteImg from "../../assets/paste.png";
import copyImg from "../../assets/copy.png";
import cutImg from "../../assets/scissors.png";
import {useAppDispatch} from "../../store/hooks/hooks.ts";
import {setText} from "../../store/slices/textSlice.ts";

const ClipBoard = () => {

    const dispatch = useAppDispatch();
    function handlePaste() {
        navigator.clipboard
            .readText()
            .then(
                text =>
                {
                    dispatch(setText(text));
                }
            );
    }
    
    return (
        <div className="clip-board">
            <div className="vertical-img-group pointer">
                <img onClick={handlePaste} className="img-45" src={pasteImg} alt="paste"/>
                <span style={{textAlign:"center"}}>Paste</span>
            </div>
            <div className="small-icons-group">
                <div className="horizontal-img-group pointer">
                    <img className="img-15" src={cutImg} alt="cut"/>
                    <span>Cut</span>
                </div>
                <div className="horizontal-img-group pointer">
                    <img className="img-15" src={copyImg} alt="copy"/>
                    <span>Copy</span>
                </div>
            </div>
        </div>
    );
};

export default ClipBoard;
