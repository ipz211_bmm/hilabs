import './FontsSection.css'
import boldImg from  '../../assets/bold.png'
import italicImg from  '../../assets/italic-font.png'
import underImg from  '../../assets/underline.png'
const Fonts = () => {
    return (
        <div className="fonts-section">
            <div className="select-group">
                <select>
                    <option value="">Italic</option>
                    <option value="">Bold</option>
                    <option value="">Arial</option>
                    <option value="">Sans-serif</option>
                </select>
                <select>
                    <option value="">5px</option>
                    <option value="">10px</option>
                    <option value="">15px</option>
                    <option value="">20px</option>
                    <option value="">25px</option>
                </select>
            </div>
            <div className="fonts-styling-group">
                <img className="img-15 pointer" src={boldImg} alt="bold"/>
                <img className="img-15 pointer" src={italicImg} alt="italic"/>
                <img className="img-15 pointer" src={underImg} alt="dots"/>
            </div>
        </div>
    );
};

export default Fonts;
