import './ParagraphSection.css'
import alignImg from "../../assets/left-text.png";
import listImg from "../../assets/list.png";
import scaleImg from "../../assets/scale.png";

const ParagraphSection = () => {
    return (
        <div className="small-icons-row">
            <img className="img-15 pointer" src={alignImg} alt="align"/>
            <img className="img-15 pointer" style={{transform:"scaleX(-1)"}} src={alignImg} alt="align"/>
            <img className="img-15 pointer" src={listImg} alt="list"/>
            <img className="img-15 pointer" src={scaleImg} alt="list"/>
        </div>
    );
};

export default ParagraphSection;
