import {ChangeEvent, useEffect, useState} from "react";
import {useAppDispatch, useTypedSelector} from "../../store/hooks/hooks.ts";
import {setText} from "../../store/slices/textSlice.ts";




const MyComponent = () => {

    const dispatch = useAppDispatch();

    const [localText,setLocalText] = useState<string>("");

    const {text,getText,replace,editorColor,search} = useTypedSelector(s=>s.text);


    useEffect(() => {
       if(text!==""&&!getText?.isFileUploading&&!getText?.isUndoAction){
           setLocalText(text);
       }

       if(text!==""&&getText?.isUndoAction){
           const lastIndex:number = localText.lastIndexOf(' ');
           let txt = "";
           if(lastIndex!==-1){
               txt=localText.slice(0,lastIndex)
           }
           setLocalText(txt);
       }
    }, [text]);

    useEffect(() => {
        dispatch(setText(localText));
    }, [getText]);
    function escapeRegExp(str:string) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    useEffect(() => {
        if(replace!==""){
            setLocalText(localText.replace(new RegExp(escapeRegExp(search), 'g'),replace));
        }
    }, [replace]);
    function handleTextAreaChange(e:ChangeEvent<HTMLTextAreaElement>){
        setLocalText(e.target.value);
    }


    return (
        <textarea  value={localText} onChange={(e)=>handleTextAreaChange(e)} placeholder="Type text here..." style={{width:"100%",resize:"none",marginTop:"10px",border:"none",outline:"none",flexGrow:"1",background:`${editorColor}`}}>

        </textarea>
    );
};

export default MyComponent;
