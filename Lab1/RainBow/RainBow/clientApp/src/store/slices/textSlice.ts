import {createSlice, PayloadAction} from "@reduxjs/toolkit";


interface TextSliceState{
    text:string,
    getText:GetText|null,
    editorColor:string,
    search:string,
    replace:string,
    mousePosition:MousePosition|null
}

interface MousePosition {
    y:number,
    x:number
}
interface GetText{
    isFileUploading:boolean,
    isUndoAction?:boolean
}

const initialState:TextSliceState = {
    text:"",
    getText:null,
    editorColor:"#ffffff",
    search:"",
    replace:"",
    mousePosition:null
}
export const textSlice = createSlice({
   name:"text",
   initialState:initialState,
   reducers:{
       setText:(state:TextSliceState,action:PayloadAction<string>)=>{
           state.text=action.payload;
       },
       appendText:(state:TextSliceState,action:PayloadAction<string>)=>{
           state.text+=action.payload;
       },
       getText:(state:TextSliceState,action:PayloadAction<GetText>)=>{
           state.getText = action.payload;
       },
       setEditorColor:(state:TextSliceState,action:PayloadAction<string>)=>{
           state.editorColor = action.payload;
       },
       textSearch:(state:TextSliceState,action:PayloadAction<string>)=>{
           state.search=action.payload;
       },

       replaceTextAct:(state:TextSliceState,action:PayloadAction<string>)=>{
           state.replace=action.payload;
       },
       setMousePosition:(state:TextSliceState,action:PayloadAction<MousePosition>)=>{
           state.mousePosition=action.payload;
       },
   }
});

export const {setText,setMousePosition,replaceTextAct,textSearch,setEditorColor,getText,appendText} = textSlice.actions;

export const textReducer = textSlice.reducer;